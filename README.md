# IDS721 Individual Project 4

[![pipeline status](https://gitlab.com/dukeaiml/IDS721/zs148_individual_4/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/zs148_individual_4/-/commits/main)

## Author
Ziyu Shi

## Requirments
Rust AWS Lambda and Step Functions
- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Orchestrate data processing pipeline

## Rust Lambda Functionality
The `calculate_frequency` can parse the input text in JSON format and calculate the frequency of characters in the text and print them in lexicographical order. The `filter_high_frequency` will gain the output from the `calculate_frequency` and filter the characters who appear no less than three times and print out in lexicographical order.

## Demo Video
The demo video shows the workflow of Step Functions and functionality of the Lambdas.
![demo](./media/demoVideo.mov)

## Project Steps
### Create Rust Lambda Project
1. Use `cargo lambda new <PROJECT_NAME>` to create lambda project.
2. Add necessary dependencies to `Cargo.toml`.
3. Implement functions in `main.rs`.

### Test Locally
1. Use `cargo lambda watch` to test lambda functions locally. To test my function, please follow the commands:
    ```
    # Test calculate_frequency
    cd calculate_frequency
    cargo lambda watch
    cargo lambda invoke --data-file input.json

    # Test filter_hgih_frequency
    cd filter_hgih_frequency
    cargo lambda watch
    cargo lambda invoke --data-file input.json
    ```

### Deploy on AWS Lambda
1. Create a role with policies `AWSLambda_FullAccess`, `AWSLambdaBasicExecutionRole`, `IAMFullAccess`.
2. Obtain the binary file by building the project:
    ```
    cargo lambda build --release
    ```
3. Make sure to set up the AWS configuration and deploy by:
    ```
    cargo lambda deploy --region <REGION> --iam-role <ROLE_ARN>
    ```
Then you can check your AWS Lambda function on AWS Lambda.

### Build Corresponding Step Functions
1. Create a new State Machine in AWS Step Functions with a proper template.
2. Implement the workflow coordinating the specific execution process of lambda functions. Here is my definition of the state machine:
    ```json
    {
        "Comment": "A workflow to calculate character frequencies and filter high frequencies.",
        "StartAt": "CalculateFrequency",
        "States": {
            "CalculateFrequency": {
                "Type": "Task",
                "Resource": "<MY_FIRST_LAMBDA_ARN>",
                "Next": "FilterHighFrequency"
            },
            "FilterHighFrequency": {
                "Type": "Task",
                "Resource": "<MY_SECOND_LAMBDA_ARN>",
                "End": true
            }
        }
    }
    ```
3. Execute the state machine with input to test if the workflow can work correctly.

### Data Processing Pipeline
1. `calculate_frequency` gets String text as input and calculates the frequency of each character in the text. The output is a `BTreeMap` variable containing the characters and their frequency and printed out in lexicographical order. The key of the output is characters, the value of the output is the frequency of the character. The output will be displayed in JSON format.
2. `filter_high_frequency` gets the output from `calculate_frequency`. Its input is a `BTreeMap` variable containing the characters and their frequency. The variable can be defined in JSON format. Then the lambda function will filter the characters who appear no less than three times in the input text. The result will be displayed in JSON format.

### Build GitLab CI/CD
Create `.gitlab-ci.yml` to build a CI/CD pipeline to build and deploy the Rust Lambda to AWS. Here is the part of my `.gitlab-ci.yml` to build CI/CD pipeline:
```yml
calculate_frequency_stage:
  stage: calculate_frequency_stage
  image: rust:latest
  script:
    - rustup default stable
    - apt-get update && apt-get install -y wget unzip xz-utils
    - wget https://ziglang.org/download/0.9.1/zig-linux-x86_64-0.9.1.tar.xz
    - tar -xf zig-linux-x86_64-0.9.1.tar.xz -C /usr/local
    - export PATH=$PATH:/usr/local/zig-linux-x86_64-0.9.1
    - cargo install cargo-lambda
    - apt-get install -y zip
    - apt-get install -y musl-tools
    - rustup target add x86_64-unknown-linux-musl
    - cd calculate_frequency
    - export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
    - export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
    - export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
    - cargo lambda build --release
    - cargo lambda deploy
  only:
    - main
```

## Srceenshots
### Deploy calculation lambda on AWS Lambda
![image](media/lambda1.png)

### Deploy filtering lambda on AWS Lambda
![image](media/lambda2.png)

### Test Single Lambda Functionality
![image](media/testLambda.png)

### State Machine Detail
![image](media/stepMachineDetail.png)

### Execution Events
![image](media/events.png)

### Execution Status
![image](media/status.png)

### Graph View of Step Function Workflow and Execution Detail
![image](media/graph.png)